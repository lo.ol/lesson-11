#include "threads.h"
/*print to screen*/
void I_Love_Threads()
{
	cout << "I Love Threads\n";
}
/*calls print i love threads with a thread*/
void call_I_Love_Threads()
{
	thread t(I_Love_Threads);
	t.join();
}
/*prints all prime nums found*/
void printVector(vector<int> primes)
{
	vector<int>::iterator it;
	for (it = primes.begin(); it != primes.end(); it++)
	{
		cout << *it << endl;
	}
}

/*gets all prime numbers between begin and end and puts them in vector*/
void getPrimes(int begin, int end, vector<int>& primes)
{
	bool flag = true;
	for (int i = begin; i <= end; i++)
	{
		for(int j = 2; j <= sqrt(i) && flag; j++)
		{
			if (i % j == 0) //if num dividable it is not prime
			{
				flag = false;
			}
		}
		if (flag)
		{
			primes.push_back(i);
		}
		flag = true;
	}

}

/*calls function get primes with a thread and prints how much time the thread worked*/
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> v;
	clock_t tStart = clock();
	thread t(getPrimes, ref(begin), ref(end), ref(v));
	t.join();
	
	
	cout << "time is: " << (double)(clock() - tStart) / CLOCKS_PER_SEC << endl;
	

	return v;
}

/*gets all prime numbers between begin and end if file is open and prints them to given file*/
void writePrimesToFile(int begin, int end, ofstream & file)
{
	
	if (file.is_open())
	{
		bool flag = true;
		for (int i = begin; i <= end; i++)
		{
			for (int j = 2; j <= sqrt(i) && flag; j++)
			{
				if (i % j == 0)
				{
					flag = false;
				}
			}
			if (flag)
			{
				file << i << " ";
			}
			flag = true;
		}
		
	}
	else
	{
		cout << "Error! file did not open!";
	}
}


void callWritePrimesMultipleThreads(int begin, int end, string filePath, const int N)
{
	int newend,diff = (end - begin) / N; //divides numbers to check to N threades 
	vector<thread> threads;
	ofstream file;

	file.open(filePath);
	
	clock_t tStart = clock();
	for (int i = begin; i <= end; i += diff+1) //create N threades
	{
		newend = i + diff;
		if (newend > end)
		{
			newend = end;
			
		}
		
		threads.push_back(thread(writePrimesToFile, ref(i), ref(newend), ref(file)));
		
		 
	}

	for (int i = 0; i < N; i++) 
	{
		threads[i].join();
	}
	cout << "time is: " << (double)(clock() - tStart) / CLOCKS_PER_SEC << endl; //prints time that took to thread
	file.close();
	
}
